package fiper

import (
	"context"
	"errors"
	"log"

	"github.com/uptrace/bun"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fbun"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fgorm"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
	"gorm.io/gorm"
)

var (
	ErrMigrationTables = errors.New("cannot migration tables")
)

// New initiates Bun FiPer
func Bun(db *bun.DB, options *fp.Options) (r *fbun.FiPer) {
	r = &fbun.FiPer{
		DB:  db,
		Ctx: context.Background(),
		FP: &fp.FP{
			Options: options,
		},
	}

	if err := r.MigrateTables(); err != nil {
		log.Fatalln(ErrMigrationTables)
	}

	return
}

// New initiates Gorm FiPer
func Gorm(db *gorm.DB, options *fp.Options) (r *fgorm.FiPer) {
	r = &fgorm.FiPer{
		DB:  db,
		Ctx: context.Background(),
		FP: &fp.FP{
			Options: options,
		},
	}

	if err := r.MigrateTables(); err != nil {
		log.Fatalln(ErrMigrationTables)
	}

	return
}
