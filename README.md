<div align="center">

![](/assets/logo_small.png "fiper")

<br>

![Go](https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white)
![JWT](https://img.shields.io/badge/JWT-black?style=for-the-badge&logo=JSON%20web%20tokens)

<br><br>
<a href="https://github.com/gofiber/awesome-fiber"><img src="https://awesome.re/mentioned-badge.svg"></a>
<a href="https://paypal.me/rodrigoodhin"><img src="https://img.shields.io/badge/donate-PayPal-blue"></a>

</div>

# FiPer - Fiber Permits

FiPer is a library that provides [Fiber](https://gofiber.io/) with Role Based Access Control (RBAC) using JWT and with database persistence.
Two ORM libraries are supported: [Gorm](https://gorm.io/) and [Bun](https://bun.uptrace.dev/).

&nbsp;
&nbsp;
&nbsp;

## Install

```
go get gitlab.com/rodrigoodhin/fiper
```

```
import (
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fgorm"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fbun"
)
```

&nbsp;
&nbsp;
&nbsp;

## Usage

To initiate FiPer you need to pass two variables for the `new`function. The first one is the [bun](https://bun.uptrace.dev/) instance, the second is the option. Option is a type and contains the follow variables:

| Name          | Description                        |
| ------------- | ---------------------------------- |
| UserTableName | User table name                    |
| UserRoleField | Role field name on User table      |
| UserIdField   | Id field name on User table        |
| jwtLocals     | Name configured for Fiber to store |

### Create FiPer Instance

Create a Bun RBAC instance

```
r = fiper.Bun(App.DB, &fp.Options{
    UserTableName: "users",
    UserPermitField: "role",
    UserIdField: "id",
    JwtLocals: "user",
})
```

Create a Gorm RBAC instance

```
r = fiper.Gorm(App.DB, &fp.Options{
    UserTableName: "users",
    UserPermitField: "role",
    UserIdField: "id",
    JwtLocals: "user",
})
```

### Declaring Models

For GORM, the permit field must be created like this:
```go
type User struct {
    ...
    Permits  datatypes.JSON  `json:"permits"`
    ...
}
```

For BUN, the permit field must be created like this:
```go
type User struct {
    ...
    Permits  []string  `json:"permits" bun:",array"`
    ...
}
```

&nbsp;
&nbsp;
&nbsp;

## Features

### Fiber Handler

```
r.CheckFiPer([]string{"admin", "role:listAll"})
```

### Roles 

Get Role
```
roles, err = r.GetRoles()
```

Add Role
```
r.AddRole(fgorm.FiperRole{
    Name: "admin",
})

r.AddRole(fbun.FiperRole{
    Name: "admin",
})
```

Delete Role
```
r.DeleteRole("admin")
```

### Permissions

Get Permission
```
permissions, err = r.GetPermissions()
```

Add Permission
```
r.AddPermission(fgorm.FiperPermission{
    Model:  "user",
    Action: "list",
})

r.AddPermission(fbun.FiperPermission{
    Model:  "user",
    Action: "list",
})
```

Delete Permission
```
r.DeletePermission("user", "list")
```

### Role Permissions

Get Role Permission
```
rolePermissions, err = r.GetRolePermissions()
```

Add Role Permission
```
r.AddRolePermission(fgorm.AddRolePermission{
    "role": "admin",
    "permissionModel": "user",
    "permissionAction": "list"
})

r.AddRolePermission(fbun.AddRolePermission{
    "role": "admin",
    "permissionModel": "user",
    "permissionAction": "list"
})
```

Delete Role Permission
```
r.DeleteRolePermission("admin", "user", "list")
```

### Users

Get User Permits
```
r.GetUserPermits(userId)
```

Grant User Role
```
r.GrantUserRole(userId, roleName)
```

Grant User Permission
```
r.GrantUserPermission(userId, permissionModel, permissionAction)
```

Revoke User Permit
```
r.RevokeUserPermit(userId, permit)
```

&nbsp;
&nbsp;
&nbsp;

### [MIT License](/LICENSE)
