module gitlab.com/rodrigoodhin/fiper

go 1.20

require github.com/gofiber/fiber/v2 v2.49.1

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.0
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/uptrace/bun v1.1.14
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.49.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
)

require (
	github.com/bytedance/sonic v1.10.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gofiber/jwt/v3 v3.2.11
	github.com/google/uuid v1.3.1
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/tmthrgd/go-hex v0.0.0-20190904060850-447a3041c3bc // indirect
	github.com/uptrace/bun/dialect/pgdialect v1.1.14
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	gorm.io/datatypes v1.2.0
	gorm.io/driver/postgres v1.5.2
	gorm.io/gorm v1.25.4
)

require (
	github.com/chenzhuoyu/base64x v0.0.0-20230717121745-296ad89f973d // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/klauspost/cpuid/v2 v2.2.5 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	github.com/twitchyliquid64/golang-asm v0.15.1 // indirect
	github.com/uptrace/bun/extra/bundebug v1.0.19
	golang.org/x/arch v0.5.0 // indirect
	golang.org/x/crypto v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	gorm.io/driver/mysql v1.5.1 // indirect
)

require (
	github.com/fatih/color v1.13.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/uptrace/bun/driver/pgdriver v1.0.19
)

require (
	github.com/chenzhuoyu/iasm v0.9.0 // indirect
	github.com/jackc/pgx/v5 v5.3.1 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	mellium.im/sasl v0.2.1 // indirect
)
