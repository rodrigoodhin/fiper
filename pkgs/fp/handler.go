package fp

import (
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
)

func (f *FP) GetUserId(c *fiber.Ctx) string {
	usr := c.Locals(f.Options.JwtLocals).(*jwt.Token)
	claims := usr.Claims.(jwt.MapClaims)
	return claims[f.Options.UserIdField].(string)
}
