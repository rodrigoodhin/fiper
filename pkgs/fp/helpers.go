package fp

func ExistPermit(list []string, ele string) bool {
	for _, x := range list {
		if x == ele {
			return true
		}
	}

	return false
}

func StringInSlice(a string, list []string) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}