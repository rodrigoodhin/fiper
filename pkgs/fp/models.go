package fp

import (
	"github.com/google/uuid"
)

type User struct {
	ID   uuid.UUID
	Role []string
}

type Options struct {
	UserTableName    string
	UserPermitsField string
	UserIdField      string
	JwtLocals        string
}

type FP struct {
	Options *Options
}
