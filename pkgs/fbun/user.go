package fbun

import (
	"github.com/uptrace/bun/dialect/pgdialect"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

// GetUserPermits - get user roles and permissions in the database
func (b *FiPer) GetUserPermits(userId string) (permits Permits, err error) {
	var pers []string

	err = b.DB.NewSelect().
		Table(b.FP.Options.UserTableName).
		ColumnExpr(b.FP.Options.UserPermitsField).
		Where(b.FP.Options.UserIdField+" = ?", userId).
		Scan(b.Ctx, pgdialect.Array(&pers))
	if err != nil {
		return permits, fp.PrintError(fp.ErrGettingUser, err)
	}

	permits.UserID = userId
	permits.Permits = pers

	return
}

// GrantUserRole - grant user role in the database
func (b *FiPer) GrantUserRole(userId, roleName string) (permits Permits, err error) {
	count, err := b.DB.NewSelect().Where("name = ?", roleName).Model(&FiperRole{}).Count(b.Ctx)
	if count == 0 || err != nil {
		return permits, fp.PrintError(fp.ErrRoleNotFound, err)
	}

	return b.grantUserPermit(userId, roleName)
}

// GrantUserPermission - grant user permission in the database
func (b *FiPer) GrantUserPermission(userId, permissionModel, permissionAction string) (permits Permits, err error) {
	count, err := b.DB.NewSelect().Where("model = ? and action = ?", permissionModel, permissionAction).Model(&FiperPermission{}).Count(b.Ctx)
	if count == 0 || err != nil {
		return permits, fp.PrintError(fp.ErrPermissionNotFound, err)
	}

	return b.grantUserPermit(userId, permissionModel+":"+permissionAction)
}

// grantUserPermit - grant user role and permission in the database
func (b *FiPer) grantUserPermit(userId, permit string) (permits Permits, err error) {
	permits, err = b.GetUserPermits(userId)
	if err != nil {
		return permits, fp.PrintError(fp.ErrGettingUserPermits, err)
	}

	if fp.ExistPermit(permits.Permits, permit) {
		return permits, fp.PrintError(fp.ErrUserAlreadyHaveThisPermit, err)
	}

	permits.Permits = append(permits.Permits, permit)

	_, err = b.DB.NewUpdate().
		Table(b.FP.Options.UserTableName).
		Set(b.FP.Options.UserPermitsField+" = ?", pgdialect.Array(permits.Permits)).
		Where(b.FP.Options.UserIdField+" = ?", userId).
		Exec(b.Ctx)
	if err != nil {
		return permits, fp.PrintError(fp.ErrGrantingPermit, err)
	}

	return
}

// RevokeUserPermit - revoke user role and permission in the database
func (b *FiPer) RevokeUserPermit(userId, permit string) (permits Permits, err error) {
	var oldPermits []string

	permits, err = b.GetUserPermits(userId)
	if err != nil {
		return permits, fp.PrintError(fp.ErrGettingUserPermits, err)
	}

	oldPermits = permits.Permits
	permits.Permits = []string{}

	for _, p := range oldPermits {
		if p != permit {
			permits.Permits = append(permits.Permits, p)
		}
	}

	_, err = b.DB.NewUpdate().
		Table(b.FP.Options.UserTableName).
		Set(b.FP.Options.UserPermitsField+" = ?", pgdialect.Array(permits.Permits)).
		Where(b.FP.Options.UserIdField+" = ?", userId).
		Exec(b.Ctx)
	if err != nil {
		return permits, fp.PrintError(fp.ErrRevokingPermit, err)
	}

	return
}
