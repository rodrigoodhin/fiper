package fbun

func (b *FiPer) MigrateTables() (err error) {
	if _, err = b.DB.NewCreateTable().
		Model(&FiperRole{}).
		IfNotExists().
		Exec(b.Ctx); err != nil {
		return
	}

	if _, err = b.DB.NewCreateTable().
		Model(&FiperPermission{}).
		IfNotExists().
		Exec(b.Ctx); err != nil {
		return
	}

	if _, err = b.DB.NewCreateTable().
		Model(&FiperRolePermission{}).
		IfNotExists().
		ForeignKey(`("role_id") REFERENCES "fiper_roles" ("id") ON DELETE CASCADE`).
		ForeignKey(`("permission_id") REFERENCES "fiper_permissions" ("id") ON DELETE CASCADE`).
		Exec(b.Ctx); err != nil {
		return
	}

	return
}
