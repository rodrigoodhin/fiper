package fbun

import (
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

// GetRolePermissions - get stored role permissions in the database
func (b *FiPer) GetRolePermissions() (rolePermissions []FiperRolePermission, err error) {
	err = b.DB.NewSelect().
		Model(&rolePermissions).
		Relation("Role").
		Relation("Permission").
		Scan(b.Ctx, &rolePermissions)
	if err != nil {
		return nil, fp.PrintError(fp.ErrGettingRolePermissions, err)
	}

	return
}

// AddRolePermission - stores a role permission in the database
func (b *FiPer) AddRolePermission(arp AddRolePermission) (rolePermission FiperRolePermission, err error) {
	var (
		role            FiperRole
		permission      FiperPermission
		countRole       int
		countPermission int
	)

	countRole, _ = b.DB.NewSelect().
		Model(&role).
		Where("name = ?", arp.Role).
		ScanAndCount(b.Ctx, &role)

	countPermission, _ = b.DB.NewSelect().
		Model(&permission).
		Where("model = ? and action = ?", arp.PermissionModel, arp.PermissionAction).
		ScanAndCount(b.Ctx, &permission)

	if countRole == 0 || countPermission == 0 {
		return FiperRolePermission{}, fp.PrintError(fp.ErrRoleOrPermissionNotFound, err)
	}

	rolePermission.RoleID = role.ID
	rolePermission.PermissionID = permission.ID

	rolePermission.Role = &FiperRole{
		ID:   role.ID,
		Name: arp.Role,
	}

	rolePermission.Permission = &FiperPermission{
		ID:     permission.ID,
		Model:  arp.PermissionModel,
		Action: arp.PermissionAction,
	}

	_, err = b.DB.NewInsert().
		Model(&rolePermission).
		Column("role_id", "permission_id").
		On("CONFLICT (role_id, permission_id) DO NOTHING").
		Exec(b.Ctx)
	if err != nil {
		return FiperRolePermission{}, fp.PrintError(fp.ErrAddingRolePermission, err)
	}

	return
}

// DeleteRolePermission - removes a stored role permission from database
func (b *FiPer) DeleteRolePermission(roleName, permissionModel, permissionAction string) (err error) {
	_, err = b.DB.NewDelete().
		Model((*FiperRolePermission)(nil)).
		TableExpr("fiper_roles AS role").
		TableExpr("fiper_permissions AS permission").
		Where("role.id = fiper_role_permission.role_id").
		Where("permission.id = fiper_role_permission.permission_id").
		Where("role.name = ? and permission.model = ? and permission.action = ?", roleName, permissionModel, permissionAction).
		Exec(b.Ctx)
	if err != nil {
		return fp.PrintError(fp.ErrDeletingRolePermission, err)
	}

	return
}
