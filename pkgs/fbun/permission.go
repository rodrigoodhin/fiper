package fbun

import (
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

// GetPermissions - get stored permissions in the database
func (b *FiPer) GetPermissions() (permissions []FiperPermission, err error) {
	err = b.DB.NewSelect().
		Model(&permissions).
		Scan(b.Ctx, &permissions)
	if err != nil {
		return nil, fp.PrintError(fp.ErrGettingPermissions, err)
	}

	return
}

// AddPermission - stores a permission in the database
func (b *FiPer) AddPermission(permission FiperPermission) (FiperPermission, error) {
	_, err := b.DB.NewInsert().
		Model(&permission).
		On("CONFLICT (model, action) DO NOTHING").
		Exec(b.Ctx)
	if err != nil {
		return FiperPermission{}, fp.PrintError(fp.ErrAddingPermissions, err)
	}

	return permission, err
}

// DeletePermission - removes a stored permission from database
func (b *FiPer) DeletePermission(permissionModel, permissionAction string) (err error) {
	_, err = b.DB.NewDelete().
		Model((*FiperPermission)(nil)).
		Where("model = ? and action = ?", permissionModel, permissionAction).
		Exec(b.Ctx)
	if err != nil {
		return fp.PrintError(fp.ErrDeletingPermissions, err)
	}

	return
}
