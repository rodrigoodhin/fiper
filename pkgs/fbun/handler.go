package fbun

import (
	"github.com/gofiber/fiber/v2"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

func (b *FiPer) CheckFiPer(rbacList []string) fiber.Handler {
	// Return middleware handler
	return func(c *fiber.Ctx) error {
		var (
			userId          string
			userPermits     []string
			permitted       bool
			rolePermissions []string
		)

		userId = b.FP.GetUserId(c)

		_ = b.DB.NewSelect().
			ColumnExpr(b.FP.Options.UserPermitsField).
			Table(b.FP.Options.UserTableName).
			Where(b.FP.Options.UserIdField+" = ?", userId).
			Scan(b.Ctx, pgdialect.Array(&userPermits))

		_ = b.DB.NewSelect().
			TableExpr("fiper_role_permissions").
			ColumnExpr("concat(fiper_permissions.model,':',fiper_permissions.action) AS PERMITS").
			Join("FULL JOIN fiper_roles ON fiper_roles.id = fiper_role_permissions.role_id").
			Join("FULL JOIN fiper_permissions ON fiper_permissions.id = fiper_role_permissions.permission_id").
			Where("fiper_roles.name IN (?)", bun.In(userPermits)).
			Scan(b.Ctx, &rolePermissions)

		userPermits = append(userPermits, rolePermissions...)

		for _, u := range userPermits {
			if fp.StringInSlice(u, rbacList) {
				permitted = true
			}
		}

		if permitted {
			return c.Next()
		} else {
			return c.SendString(fp.ErrAccessNotPermitted)
		}
	}
}
