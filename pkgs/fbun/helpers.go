package fbun

import (
	"fmt"

	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

func (b *FiPer) ValidatePermits(userPermits []string) error {
	roleList := []FiperRole{}
	b.DB.NewSelect().Model(&FiperRole{}).Scan(b.Ctx, &roleList)

	roles := []string{}
	for _, role := range roleList {
		roles = append(roles, role.Name)
	}

	permissionList := []FiperPermission{}
	b.DB.NewSelect().Model(&FiperPermission{}).Scan(b.Ctx, &permissionList)

	permissions := []string{}
	for _, permission := range permissionList {
		permissions = append(permissions, permission.Model+":"+permission.Action)
	}

	for _, permit := range userPermits {
		if !fp.StringInSlice(permit, roles) && !fp.StringInSlice(permit, permissions) {
			return fp.PrintError(fp.ErrPermitNotFound, fmt.Errorf(""))
		}
	}

	return nil
}
