package fbun

import (
	"context"

	"github.com/google/uuid"
	"github.com/uptrace/bun"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

type FiPer struct {
	DB  *bun.DB
	Ctx context.Context
	FP  *fp.FP
}

type UserPermit struct {
	UserID  string   `json:"user_id" bun:"user_id"`
	Permits []string `json:"permits" bun:"permits,array"`
}

type Permits struct {
	UserID  string   `json:"user_id" bun:"user_id"`
	Name    string   `json:"name" bun:"name"`
	Model   string   `json:"model" bun:"model"`
	Action  string   `json:"action" bun:"action"`
	Permits []string `json:"permits" bun:"permits,array"`
}

// Role represents the database model of roles
type FiperRole struct {
	bun.BaseModel `bun:"table:fiper_roles"`
	ID            uuid.UUID `bun:"id,pk,type:uuid,default:uuid_generate_v4()"`
	Name          string    `bun:"name,unique,notnull"`
}

// Permission represents the database model of permissions
type FiperPermission struct {
	bun.BaseModel `bun:"table:fiper_permissions"`
	ID            uuid.UUID `bun:"id,pk,type:uuid,default:uuid_generate_v4()"`
	Model         string    `bun:"model,notnull,unique:group_permission"`
	Action        string    `bun:"action,notnull,unique:group_permission"`
}

// RolePermission represents the database that stores the relationship between roles and permissions
type FiperRolePermission struct {
	bun.BaseModel `bun:"table:fiper_role_permissions"`
	RoleID        uuid.UUID        `json:"-" bun:"role_id,pk,notnull,type:uuid,unique:group_role_permission"`
	Role          *FiperRole       `bun:"rel:belongs-to,join:role_id=id"`
	PermissionID  uuid.UUID        `json:"-" bun:"permission_id,pk,notnull,type:uuid,unique:group_role_permission"`
	Permission    *FiperPermission `bun:"rel:belongs-to,join:permission_id=id"`
}

// AddRolePermission represents a model used to store RolePermission
type AddRolePermission struct {
	Role             string
	PermissionModel  string
	PermissionAction string
}
