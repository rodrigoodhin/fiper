package fbun

import (
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

// GetRoles - get stored roles in the database
func (b *FiPer) GetRoles() (roles []FiperRole, err error) {
	err = b.DB.NewSelect().
		Model(&roles).
		Scan(b.Ctx, &roles)
	if err != nil {
		return nil, fp.PrintError(fp.ErrGettingRoles, err)
	}

	return
}

// AddRole - stores a role in the database
func (b *FiPer) AddRole(role FiperRole) (FiperRole, error) {
	_, err := b.DB.NewInsert().
		Model(&role).
		On("CONFLICT (name) DO NOTHING").
		Exec(b.Ctx)
	if err != nil {
		return FiperRole{}, fp.PrintError(fp.ErrAddingRoles, err)
	}

	return role, err
}

// DeleteRole  - removes a stored role from database
func (b *FiPer) DeleteRole(roleName string) (err error) {
	_, err = b.DB.NewDelete().
		Model((*FiperRole)(nil)).
		Where("name = ?", roleName).
		Exec(b.Ctx)
	if err != nil {
		return fp.PrintError(fp.ErrDeletingRoles, err)
	}

	return
}
