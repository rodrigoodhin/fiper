package fgorm

import (
	"fmt"

	json "github.com/bytedance/sonic"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

func (g *FiPer) CheckFiPer(rbacList []string) fiber.Handler {
	// Return middleware handler
	return func(c *fiber.Ctx) error {
		var (
			userId          string
			userPermits     []string
			permitted       bool
			permits         string
			rolePermissions []string
		)

		userId = g.FP.GetUserId(c)

		err := g.DB.Table(g.FP.Options.UserTableName).
			Select(g.FP.Options.UserPermitsField).
			Where(g.FP.Options.UserIdField+" = ?", userId).
			Find(&permits).Error
		if err != nil {
			fmt.Println(err)
		}

		err = json.Unmarshal([]byte(permits), &userPermits)
		if err != nil {
			fmt.Println(err)
		}

		_ = g.DB.Model(&FiperRolePermission{}).
			Select("concat(permission.model, ':', permission.action)").
			Joins("FULL JOIN fiper_roles as role ON role.id = fiper_role_permissions.role_id").
			Joins("FULL JOIN fiper_permissions as permission ON permission.id = fiper_role_permissions.permission_id").
			Where("role.name IN ?", userPermits).
			Find(&rolePermissions)

		userPermits = append(userPermits, rolePermissions...)

		for _, u := range userPermits {
			if fp.StringInSlice(u, rbacList) {
				permitted = true
			}
		}

		if permitted {
			return c.Next()
		} else {
			return c.SendString(fp.ErrAccessNotPermitted)
		}
	}
}
