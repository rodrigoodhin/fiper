package fgorm

import (
	json "github.com/bytedance/sonic"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

// GetUserPermits - get user roles and permissions in the database
func (g *FiPer) GetUserPermits(userId string) (permits Permits, err error) {
	userPermits := &UserPermit{}

	err = g.DB.Table(g.FP.Options.UserTableName).
		Select(g.FP.Options.UserIdField+" as user_id", g.FP.Options.UserPermitsField+" as permits").
		Where(g.FP.Options.UserIdField+" = ?", userId).
		Find(&userPermits).Error
	if err != nil {
		return permits, fp.PrintError(fp.ErrGettingUser, err)
	}

	if userPermits.UserID == "" {
		return permits, fp.PrintError(fp.ErrUserNotFound, err)
	}

	permits.UserID = userPermits.UserID
	permits.Permits = userPermits.Permits

	if len(permits.Permits) > 0 {
		err = json.Unmarshal(permits.Permits, &permits.PermitList)
		if err != nil {
			return permits, fp.PrintError(fp.ErrUnmarshallingPermits, err)
		}
	}

	return
}

// GrantUserRole - grant user role in the database
func (g *FiPer) GrantUserRole(userId, roleName string) (permits Permits, err error) {
	res := g.DB.Where("name = ?", roleName).First(&FiperRole{})
	if res.RowsAffected == 0 || res.Error != nil {
		err = res.Error
		return permits, fp.PrintError(fp.ErrRoleNotFound, err)
	}

	return g.grantUserPermit(userId, roleName)
}

// GrantUserPermission - grant user permission in the database
func (g *FiPer) GrantUserPermission(userId, permissionModel, permissionAction string) (permits Permits, err error) {
	res := g.DB.Where("model = ? and action = ?", permissionModel, permissionAction).First(&FiperPermission{})
	if res.RowsAffected == 0 || res.Error != nil {
		err = res.Error
		return permits, fp.PrintError(fp.ErrPermissionNotFound, err)
	}

	return g.grantUserPermit(userId, permissionModel+":"+permissionAction)
}

// grantUserPermit - grant user role and permission in the database
func (g *FiPer) grantUserPermit(userId, permit string) (permits Permits, err error) {
	permits, err = g.GetUserPermits(userId)
	if err != nil {
		return permits, fp.PrintError(fp.ErrGettingUserPermits, err)
	}

	if fp.ExistPermit(permits.PermitList, permit) {
		return permits, fp.PrintError(fp.ErrUserAlreadyHaveThisPermit, err)
	}

	permits.PermitList = append(permits.PermitList, permit)

	permits.Permits, err = json.Marshal(permits.PermitList)
	if err != nil {
		return permits, fp.PrintError(fp.ErrMarshallingPermits, err)
	}

	err = g.DB.Table(g.FP.Options.UserTableName).
		Where(g.FP.Options.UserIdField+" = ?", userId).
		Updates(map[string]interface{}{g.FP.Options.UserPermitsField: permits.Permits}).Error
	if err != nil {
		return permits, fp.PrintError(fp.ErrGrantingPermit, err)
	}

	return
}

// RevokeUserPermit - revoke user role and permission in the database
func (g *FiPer) RevokeUserPermit(userId, permit string) (permits Permits, err error) {
	var oldPermits []string

	permits, err = g.GetUserPermits(userId)
	if err != nil {
		return permits, fp.PrintError(fp.ErrGettingUserPermits, err)
	}

	oldPermits = permits.PermitList
	permits.PermitList = []string{}

	for _, p := range oldPermits {
		if p != permit {
			permits.PermitList = append(permits.PermitList, p)
		}
	}

	permits.Permits, err = json.Marshal(permits.PermitList)
	if err != nil {
		return permits, fp.PrintError(fp.ErrMarshallingPermits, err)
	}

	err = g.DB.Table(g.FP.Options.UserTableName).
		Where(g.FP.Options.UserIdField+" = ?", userId).
		Updates(map[string]interface{}{g.FP.Options.UserPermitsField: permits.Permits}).Error
	if err != nil {
		return permits, fp.PrintError(fp.ErrRevokingPermit, err)
	}

	return
}
