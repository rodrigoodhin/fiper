package fgorm

import (
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

// GetPermissions - get stored permissions in the database
func (g *FiPer) GetPermissions() (permissions []FiperPermission, err error) {
	err = g.DB.Find(&permissions).Error
	if err != nil {
		return nil, fp.PrintError(fp.ErrGettingPermissions, err)
	}

	return
}

// AddPermission - stores a permission in the database
func (g *FiPer) AddPermission(permission FiperPermission) (FiperPermission, error) {
	err := g.DB.Where("concat(fiper_permissions.model,':',fiper_permissions.action) = ?", permission.Model+":"+permission.Action).FirstOrCreate(&permission).Error
	if err != nil {
		return FiperPermission{}, fp.PrintError(fp.ErrAddingPermissions, err)
	}

	return permission, err
}

// DeletePermission - removes a stored permission from database
func (g *FiPer) DeletePermission(permissionModel, permissionAction string) (err error) {
	err = g.DB.Unscoped().Where("model = ? and action = ?", permissionModel, permissionAction).Delete(&FiperPermission{}).Error
	if err != nil {
		return fp.PrintError(fp.ErrDeletingPermissions, err)
	}

	return
}
