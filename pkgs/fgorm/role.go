package fgorm

import (
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

// GetRoles - get stored roles in the database
func (g *FiPer) GetRoles() (roles []FiperRole, err error) {
	err = g.DB.Find(&roles).Error
	if err != nil {
		return nil, fp.PrintError(fp.ErrGettingRoles, err)
	}

	return
}

// AddRole - stores a role in the database
func (g *FiPer) AddRole(role FiperRole) (FiperRole, error) {
	err := g.DB.Where("name = ?", role.Name).FirstOrCreate(&role).Error
	if err != nil {
		return FiperRole{}, fp.PrintError(fp.ErrAddingRoles, err)
	}

	return role, err
}

// DeleteRole  - removes a stored role from database
func (g *FiPer) DeleteRole(roleName string) (err error) {
	err = g.DB.Unscoped().Where("name = ?", roleName).Delete(&FiperRole{}).Error
	if err != nil {
		return fp.PrintError(fp.ErrDeletingRoles, err)
	}

	return
}
