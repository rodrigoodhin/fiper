package fgorm

func (g *FiPer) MigrateTables() (err error) {
	if err = g.DB.AutoMigrate(&FiperRole{}); err != nil {
		return
	}

	if err = g.DB.AutoMigrate(&FiperPermission{}); err != nil {
		return
	}

	if err = g.DB.AutoMigrate(&FiperRolePermission{}); err != nil {
		return
	}

	return
}
