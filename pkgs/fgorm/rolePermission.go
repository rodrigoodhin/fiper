package fgorm

import (
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
	"gorm.io/gorm/clause"
)

// GetRolePermissions - get stored role permissions in the database
func (g *FiPer) GetRolePermissions() (rolePermissions []FiperRolePermission, err error) {
	err = g.DB.Preload(clause.Associations).Find(&rolePermissions).Error
	if err != nil {
		return nil, fp.PrintError(fp.ErrGettingRolePermissions, err)
	}

	return
}

// AddRolePermission - stores a role permission in the database
func (g *FiPer) AddRolePermission(arp AddRolePermission) (rolePermission FiperRolePermission, err error) {
	var (
		role       FiperRole
		permission FiperPermission
	)

	resRole := g.DB.Where("name = ?", arp.Role).First(&role)

	resPermission := g.DB.Where("model = ? and action = ?", arp.PermissionModel, arp.PermissionAction).First(&permission)

	if resRole.RowsAffected == 0 || resPermission.RowsAffected == 0 {
		return FiperRolePermission{}, fp.PrintError(fp.ErrRoleOrPermissionNotFound, err)
	}

	rolePermission.Role = &role
	rolePermission.Permission = &permission

	err = g.DB.Clauses(clause.OnConflict{DoNothing: true}).Create(&rolePermission).Error
	if err != nil {
		return FiperRolePermission{}, fp.PrintError(fp.ErrAddingRolePermission, err)
	}

	return
}

// DeleteRolePermission - removes a stored role permission from database
func (g *FiPer) DeleteRolePermission(roleName, permissionModel, permissionAction string) (err error) {
	var (
		role       FiperRole
		permission FiperPermission
	)

	if err = g.DB.Where("name = ?", roleName).First(&role).Error; err != nil {
		return
	}

	if err = g.DB.Where("model = ? and action = ?", permissionModel, permissionAction).First(&permission).Error; err != nil {
		return
	}

	err = g.DB.Unscoped().Where("role_id = ? and permission_id = ?", role.ID, permission.ID).Delete(&FiperRolePermission{}).Error
	if err != nil {
		return fp.PrintError(fp.ErrDeletingRolePermission, err)
	}

	return
}
