package fgorm

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type FiPer struct {
	DB  *gorm.DB
	Ctx context.Context
	FP  *fp.FP
}

type UserPermit struct {
	UserID  string         `json:"user_id" gorm:"column:user_id"`
	Permits datatypes.JSON `json:"permits" gorm:"column:permits"`
}

type Permits struct {
	UserID     string         `json:"user_id" gorm:"column:user_id"`
	Name       string         `json:"name" gorm:"column:name"`
	Model      string         `json:"model" gorm:"column:model"`
	Action     string         `json:"action" gorm:"column:action"`
	Permits    datatypes.JSON `json:"permits" gorm:"column:permits"`
	PermitList []string       `json:"permit_list" gorm:"column:permit_list"`
}

// Role represents the database model of roles
type FiperRole struct {
	ID   uuid.UUID `gorm:"primaryKey;type:uuid;default:uuid_generate_v4()"`
	Name string    `gorm:"unique;not null"`
}

// Permission represents the database model of permissions
type FiperPermission struct {
	ID     uuid.UUID `gorm:"primaryKey;type:uuid;default:uuid_generate_v4()"`
	Model  string    `gorm:"not null"`
	Action string    `gorm:"not null"`
}

// RolePermission represents the database that stores the relationship between roles and permissions
type FiperRolePermission struct {
	RoleID       uuid.UUID        `json:"-" gorm:"primaryKey;type:uuid"`
	Role         *FiperRole       `gorm:"foreignKey:RoleID;references:ID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	PermissionID uuid.UUID        `json:"-" gorm:"primaryKey;type:uuid"`
	Permission   *FiperPermission `gorm:"foreignKey:PermissionID;references:ID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
}

// AddRolePermission represents a model used to store RolePermission
type AddRolePermission struct {
	Role             string
	PermissionModel  string
	PermissionAction string
}
