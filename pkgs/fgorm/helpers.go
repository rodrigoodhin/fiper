package fgorm

import (
	"fmt"

	json "github.com/bytedance/sonic"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
	"gorm.io/datatypes"
)

func (g *FiPer) ValidatePermits(userPermits datatypes.JSON) error {
	roleList := []FiperRole{}
	g.DB.Find(&roleList)

	roles := []string{}
	for _, role := range roleList {
		roles = append(roles, role.Name)
	}

	permissionList := []FiperPermission{}
	g.DB.Find(&permissionList)

	permissions := []string{}
	for _, permission := range permissionList {
		permissions = append(permissions, permission.Model+":"+permission.Action)
	}

	permits := []string{}
	if len(userPermits) > 0 {
		err := json.Unmarshal(userPermits, &permits)
		if err != nil {
			return fp.PrintError(fp.ErrUnmarshallingPermits, err)
		}
	}

	for _, permit := range permits {
		if !fp.StringInSlice(permit, roles) && !fp.StringInSlice(permit, permissions) {
			return fp.PrintError(fp.ErrPermitNotFound, fmt.Errorf(""))
		}
	}

	return nil
}
