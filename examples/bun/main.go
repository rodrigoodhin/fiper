package main

import (
	"context"
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/rodrigoodhin/fiper"
	"gitlab.com/rodrigoodhin/fiper/examples/bun/server"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fbun"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
)

var (
	App = &server.Data{}
)

func main() {
	service := fiber.New()

	App.Ctx = context.Background()

	App.InitDB()

	App.InitTokenCache()

	App.FP = fiper.Bun(App.DB, &fp.Options{
		UserTableName:    "users",
		UserPermitsField: "permits",
		UserIdField:      "id",
		JwtLocals:        "user",
	})

	App.FP.AddRole(fbun.FiperRole{
		Name: "admin",
	})

	App.FP.AddPermission(fbun.FiperPermission{
		Model: "book",
		Action: "all",
	})

	App.FP.AddPermission(fbun.FiperPermission{
		Model: "book",
		Action: "get",
	})

	App.FP.AddPermission(fbun.FiperPermission{
		Model: "book",
		Action: "get",
	})

	App.FP.AddRolePermission(fbun.AddRolePermission{
		Role: "admin",
		PermissionModel: "book",
		PermissionAction: "all",
	})

	App.FP.AddRolePermission(fbun.AddRolePermission{
		Role: "admin",
		PermissionModel: "book",
		PermissionAction: "get",
	})

	service.Get("/", HelloWorld)

	App.Api = service.Group("api")
	App.SetupAuthRoutes(App.Api.Group("auth"))
	App.SetupBookRoutes(App.Api.Group("book"))

	App.MigrateDB()
	
	if App.Err = service.Listen(":2387"); App.Err != nil {
		log.Fatalln(App.Err.Error())
	}
}

func HelloWorld(c *fiber.Ctx) (err error){
	return c.Status(200).SendString("Hello, World!")
}