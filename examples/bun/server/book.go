package server

import (
	"github.com/gofiber/fiber/v2"
)

func (d *Data) SetupBookRoutes(router fiber.Router) {
	router.Get("/",
		d.IsRequired(),
		d.FP.CheckFiPer([]string{"admin", "book:all"}),
		d.GetBooks)
	router.Get("/:id",
		d.IsRequired(),
		d.FP.CheckFiPer([]string{"admin", "book:get"}),
		d.GetBook)
	router.Post("/",
		d.IsRequired(),
		d.FP.CheckFiPer([]string{"admin", "book:new"}),
		d.NewBook)
	router.Delete("/:id",
		d.IsRequired(),
		d.FP.CheckFiPer([]string{"admin", "book:delete"}),
		d.DeleteBook)
}

func (d *Data) GetBooks(c *fiber.Ctx) (err error) {
	return c.Status(200).SendString("All Books")
}

func (d *Data) GetBook(c *fiber.Ctx) (err error) {
	return c.Status(200).SendString("Single Book")
}

func (d *Data) NewBook(c *fiber.Ctx) (err error) {
	return c.Status(200).SendString("New Book")
}

func (d *Data) DeleteBook(c *fiber.Ctx) (err error) {
	return c.Status(200).SendString("Delete Book")
}
