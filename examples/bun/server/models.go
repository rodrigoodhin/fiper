package server

import (
	"context"
	"time"

	"github.com/go-redis/redis"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/uptrace/bun"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fbun"
)

type Data struct {
	DB         *bun.DB
	Api        fiber.Router
	FP         *fbun.FiPer
	Ctx        context.Context
	TokenCache *redis.Client
	Err        error
}

type User struct {
	ID       uuid.UUID `json:"id" bun:",pk,type:uuid,default:uuid_generate_v4()"`
	Email    string    `json:"email" bun:",unique,notnull"`
	Password string    `json:"password"`
	Permits  []string  `json:"permits" bun:",array"`
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// SessionInfo -  Sessin info Data
type SessionInfo struct {
	Browser string
	IP      string
	System  string
}

// TokenCache - Token Cache fields
type TokenCache struct {
	UserID      uuid.UUID `json:"userID"`
	UserPermits []string  `json:"userPermits"`
	CreatedAt   time.Time `json:"createdAt"`
	ExpireAt    time.Time `json:"expireAt"`
}
