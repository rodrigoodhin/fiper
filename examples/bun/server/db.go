package server

import (
	"database/sql"
	"log"

	"github.com/google/uuid"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"github.com/uptrace/bun/extra/bundebug"
)

func (d *Data) InitDB() {
	sqldb := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN("postgres://postgres:fiper-root-pass@localhost:12301/postgres?sslmode=disable")))

	d.DB = bun.NewDB(sqldb, pgdialect.New())

	d.DB.AddQueryHook(bundebug.NewQueryHook(bundebug.WithVerbose(true)))
}

func (d *Data) MigrateDB() {
	if _, d.Err = d.DB.NewCreateTable().
		Model(&User{}).
		IfNotExists().
		Exec(d.Ctx); 
		d.Err != nil {
		log.Panicf("failed to migrate user model: %s", d.Err.Error())
	}

	user := &User{
		ID:       uuid.Must(uuid.Parse("697c3d75-447b-498d-b860-e6f2b873d208")),
		Email:    "admin@fiper.io",
		Password: "pass2387",
		Permits:  []string{"admin"},
	}

	if _, d.Err = d.DB.NewInsert().
		Model(user).
		On("CONFLICT (email) DO NOTHING").
		Exec(d.Ctx); d.Err != nil {
		log.Panicf("failed to create user user: %s", d.Err.Error())
	}

	user = &User{
		ID:       uuid.Must(uuid.Parse("e6f2b873-447b-498d-b860-697c3d75d208")),
		Email:    "customer@fiper.io",
		Password: "pass2387",
		Permits:  []string{"book:all", "book:get"},
	}

	if _, d.Err = d.DB.NewInsert().
		Model(user).
		On("CONFLICT (email) DO NOTHING").
		Exec(d.Ctx); d.Err != nil {
		log.Panicf("failed to create cusgtomer user: %s", d.Err.Error())
	}
}

func (d Data) LoginQuery(email, password string) (user User, err error) {
	err = d.DB.NewSelect().
		Column("id", "permits", "password").
		Model(&user).
		Where("email = ? and password = ?", email, password).
		Scan(d.Ctx, &user)
	if err != nil {
		return
	}

	return
}
