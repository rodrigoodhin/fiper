package server

import (
	"github.com/go-redis/redis"
)

// InitTokenCache - Init data store instance to save users tokens info
func (d *Data) InitTokenCache() {

	d.TokenCache = redis.NewClient(&redis.Options{
		Addr:     "localhost:12303",
		Password: "",
		DB:       0,
	})

	return
}