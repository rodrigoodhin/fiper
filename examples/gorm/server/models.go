package server

import (
	"context"
	"time"

	"github.com/go-redis/redis"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fgorm"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type Data struct {
	DB         *gorm.DB
	Api        fiber.Router
	FP         *fgorm.FiPer
	Ctx        context.Context
	TokenCache *redis.Client
	Err        error
}

type User struct {
	ID       uuid.UUID      `json:"id" gorm:"primaryKey;type:uuid;default:uuid_generate_v4();unique"`
	Email    string         `json:"email" gorm:"not null;unique"`
	Password string         `json:"password"`
	Permits  datatypes.JSON `json:"permits"`
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// SessionInfo -  Sessin info Data
type SessionInfo struct {
	Browser string
	IP      string
	System  string
}

// TokenCache - Token Cache fields
type TokenCache struct {
	UserID      uuid.UUID      `json:"userID"`
	UserPermits datatypes.JSON `json:"userPermits"`
	CreatedAt   time.Time      `json:"createdAt"`
	ExpireAt    time.Time      `json:"expireAt"`
}
