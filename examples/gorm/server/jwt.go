package server

import (
	"log"
	"time"

	json "github.com/bytedance/sonic"
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"github.com/golang-jwt/jwt/v4"
)

func (d Data) IsRequired() fiber.Handler {
	return jwtware.New(jwtware.Config{
		SigningKey:     []byte(JWTSecret),
		ErrorHandler:   d.jwtError,
		SuccessHandler: d.jwtSuccess,
	})
}

// jwtError - Handler error for authentication middleware
func (d Data) jwtError(c *fiber.Ctx, err error) error {
	if err.Error() == "Missing or malformed JWT" {
		return c.Status(400).JSON(fiber.Map{"msg": "Missing or malformed token", "error": err.Error()})
	}

	return c.Status(401).JSON(fiber.Map{"msg": "Invalid or expired token", "error": err.Error()})
}

// jwtSuccess - Handler success for authentication middleware
func (d Data) jwtSuccess(c *fiber.Ctx) error {
	// Get user token information saved at data store
	tokenCache, err := d.getTokenCache(c)
	if err != nil {
		return c.Status(401).JSON(fiber.Map{"msg": "Invalid token", "error": err.Error()})
	}

	// Compare token value with user id
	if tokenCache.UserID.String() != d.getUserId(c) {
		return c.Status(401).JSON(fiber.Map{"msg": "Invalid token", "error": err.Error()})
	}

	// Go to the next middleware
	return c.Next()
}

// getReqTokenRaw - Get token from request
func (d Data) getReqTokenRaw(c *fiber.Ctx) string {
	return c.Locals("user").(*jwt.Token).Raw
}

// getClaims - Get claims from request
func (d Data) getClaims(c *fiber.Ctx) (claims jwt.MapClaims) {
	usr := c.Locals("user").(*jwt.Token)
	claims = usr.Claims.(jwt.MapClaims)
	return
}

// getTokenCache - Get user token information saved at data store
func (d Data) getTokenCache(c *fiber.Ctx) (tc *TokenCache, err error) {
	tokenCacheValue, err := d.TokenCache.Get(d.getReqTokenRaw(c)).Result()

	if tokenCacheValue != "" {
		err = json.Unmarshal([]byte(tokenCacheValue), &tc)
	}

	return
}

// getUserId - Ger User Id from claims
func (d Data) getUserId(c *fiber.Ctx) string {
	return d.getClaims(c)["id"].(string)
}

// generateToken - Generate a new token
func (d Data) generateToken(user *User, secret string, si *SessionInfo) (t string, err error) {
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set session timeout
	sessionTimeout := d.getSessionTimeout()

	// Prepare dates
	timeNow := time.Now()
	expireAt := timeNow.Add(sessionTimeout)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = user.ID
	claims["exp"] = expireAt.Unix()

	// Generate encoded token and send it as response.
	t, err = token.SignedString([]byte(secret))
	if err != nil {
		log.Fatalf("Failed to generate a new token: %v", err)
	}

	// Create token cache instance
	tokenCache := new(TokenCache)
	tokenCache.UserID = user.ID
	tokenCache.UserPermits = user.Permits
	tokenCache.CreatedAt = timeNow
	tokenCache.ExpireAt = expireAt

	// Marshal token cache
	tc, err := json.Marshal(tokenCache)
	if err != nil {
		log.Fatalf("Failed to parse token cache to map: %v", err)
	}

	// Save token in data store
	err = d.TokenCache.Set(t, string(tc), sessionTimeout).Err()
	if err != nil {
		log.Fatalf("Failed to call Put: %v", err)
	}

	return
}

// getSessionTimeout - Calculate session timeout
func (d Data) getSessionTimeout() time.Duration {
	return 120 * time.Minute
}
