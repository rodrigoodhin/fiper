package server

import (
	"github.com/gofiber/fiber/v2"
)

func (d *Data) SetupAuthRoutes(router fiber.Router) {
	router.Post("/",
		d.Login)
	router.Delete("/:id",
		d.IsRequired(),
		d.Logout)
}

func (d *Data) Login(c *fiber.Ctx) (err error) {
	loginRequest := new(LoginRequest)
	err = c.BodyParser(loginRequest)
	if err != nil {
		return c.Status(400).JSON(fiber.Map{"msg": "Request body is invalid", "error": err.Error()})
	}

	var user User

	// validate if user exits in db
	if user, err = d.LoginQuery(loginRequest.Email, loginRequest.Password); err != nil {
		return c.Status(401).JSON(fiber.Map{"msg": "Invalid username or password", "error": err.Error()})
	}

	// Generate encoded token and send it as response.
	t, err := d.generateToken(&user, JWTSecret, &SessionInfo{})
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"msg": "Error generating token", "error": err.Error()})
	}

	return c.Status(200).JSON(fiber.Map{"token": t})
}

func (d *Data) Logout(c *fiber.Ctx) (err error) {
	err = d.TokenCache.Del(d.getReqTokenRaw(c)).Err()
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"msg": "Error deleting token", "error": err.Error()})
	}

	return c.Status(200).JSON("")
}
