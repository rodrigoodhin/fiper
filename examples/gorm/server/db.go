package server

import (
	"log"

	"github.com/google/uuid"
	"gorm.io/datatypes"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func (d *Data) InitDB() {
	dsn := "host=localhost user=postgres password=fiper-root-pass dbname=postgres port=12301 sslmode=disable TimeZone=Europe/Lisbon"
	if d.DB, d.Err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger:      logger.Default.LogMode(logger.Info),
		PrepareStmt: false,
	}); d.Err != nil {
		log.Panicf("failed to connect database: %s", d.Err.Error())
	}
}

func (d *Data) MigrateDB() {
	if d.Err = d.DB.AutoMigrate(&User{}); d.Err != nil {
		log.Panicf("failed to migrate user model: %s", d.Err.Error())
	}

	user := &User{
		ID:       uuid.Must(uuid.Parse("697c3d75-447b-498d-b860-e6f2b873d208")),
		Email:    "admin@fiper.io",
		Password: "pass2387",
		Permits:  datatypes.JSON([]byte(`["admin"]`)),
	}

	if d.Err = d.DB.Where("id = ? OR email = ?", user.ID, user.Email).FirstOrCreate(&user).Error; d.Err != nil {
		log.Panicf("failed to create admin user: %s", d.Err.Error())
	}

	user = &User{
		ID:       uuid.Must(uuid.Parse("e6f2b873-447b-498d-b860-697c3d75d208")),
		Email:    "customer@fiper.io",
		Password: "pass2387",
		Permits:  datatypes.JSON([]byte(`["book:all","book:get"]`)),
	}

	if d.Err = d.DB.Where("id = ? OR email = ?", user.ID, user.Email).FirstOrCreate(&user).Error; d.Err != nil {
		log.Panicf("failed to create customer user: %s", d.Err.Error())
	}
}

func (d Data) LoginQuery(email, password string) (user User, err error) {
	if err = d.DB.Select("id", "permits", "password").
		Where("email = ? and password = ?", email, password).
		First(&user).Error; err != nil {
		return
	}

	return
}
