package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/rodrigoodhin/fiper/examples/gorm/server"
	"gitlab.com/rodrigoodhin/fiper"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fgorm"
)

var (
	App = &server.Data{}
)

func main() {
	service := fiber.New()

	App.InitDB()

	App.InitTokenCache()

	App.FP = fiper.Gorm(App.DB, &fp.Options{
		UserTableName:    "users",
		UserPermitsField: "permits",
		UserIdField:      "id",
		JwtLocals:        "user",
	})

	App.FP.AddRole(fgorm.FiperRole{
		Name: "admin",
	})

	App.FP.AddPermission(fgorm.FiperPermission{
		Model: "book",
		Action: "all",
	})

	App.FP.AddPermission(fgorm.FiperPermission{
		Model: "book",
		Action: "get",
	})

	App.FP.AddPermission(fgorm.FiperPermission{
		Model: "book",
		Action: "get",
	})

	App.FP.AddRolePermission(fgorm.AddRolePermission{
		Role: "admin",
		PermissionModel: "book",
		PermissionAction: "all",
	})

	App.FP.AddRolePermission(fgorm.AddRolePermission{
		Role: "admin",
		PermissionModel: "book",
		PermissionAction: "get",
	})

	service.Get("/", HelloWorld)

	App.Api = service.Group("api")
	App.SetupAuthRoutes(App.Api.Group("auth"))
	App.SetupBookRoutes(App.Api.Group("book"))

	App.MigrateDB()
	
	if App.Err = service.Listen(":2387"); App.Err != nil {
		log.Fatalln(App.Err.Error())
	}
}

func HelloWorld(c *fiber.Ctx) (err error){
	return c.Status(200).SendString("Hello, World!")
}